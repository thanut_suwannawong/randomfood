from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest, time

class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        #self.browser.implicitly_wait(3)
        time.sleep(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Edith has heard about a cool new online to-do app. She goes
        # to check out its homepage
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(5)

        # She notices the page title and header mention to-do lists
        self.assertIn('To-Do', self.browser.title)
        
if __name__ == '__main__':  
    unittest.main(warnings='ignore')  