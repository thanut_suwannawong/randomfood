from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.utils import timezone
from randomfood.models import *
import time

class NewVisitorTest(LiveServerTestCase):
    
    def setUp(self):  
        self.browser = webdriver.Firefox()
        self.create_test_data()
        time.sleep(3)


    def tearDown(self):
        self.browser.quit()
        
    def create_test_data(self):
        q = Food.objects.create(food_text="หมวดข้าวจานเดียว",pub_date=timezone.now())
        q.save()
        q.menu_set.create(food_name="ข้าวผัดหมู",food_cal="350")

    def test_random_result(self):  
        # Mr.Wick want to have dinner
        # he goes to restaurant
        # he don't know what to eat
        # he've heard about new app "Random_food"
        # app will random food for the one who doesn't know what to have for that meal
        # he goes in to website
        self.browser.get(self.live_server_url)
        # he sees the header "Welcome to Random_food"
        self.assertIn('Welcome to Random_food', self.browser.title)  
        time.sleep(1)
        # he clicks on button
        # the web site change its page
        button =  self.browser.find_element_by_id('button_box')
        button.click()
        time.sleep(3)
        
        # he sees the new header
        # self.assertIn('Fill the information', self.browser.title)
        # he sees many options of food
        # he selects one of those choices
        self.browser.find_element_by_id("choice").click()
        submit = self.browser.find_element_by_id("submit")
        submit.send_keys(Keys.ENTER)
        
        time.sleep(5)
        self.fail('Finish the test!')
        
if __name__ == '__main__':  
    unittest.main(warnings='ignore')  