from django.conf.urls import url
from . import views

app_name = 'randomfood'
urlpatterns = [
    url(r'^$', views.fill_detail, name='fill_detail'),
    url(r'^random_food_by_type$', views.random_food_by_type, name='random_food_by_type'),
    url(r'^edit_list$', views.edit_list, name='edit_list'),
    url(r'^edit_option$', views.edit_option, name='edit_option'),
    url(r'^add_list/(?P<food_id>[0-9]+)/$', views.add_list, name='add_list'),
    url(r'^del_list/(?P<food_id>[0-9]+)/$', views.del_list, name='del_list'),
    ]
