import datetime
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
    
@python_2_unicode_compatible  # only if you need to support Python 2
class Food(models.Model):
    food_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    
    def __str__(self):
        return self.food_type

@python_2_unicode_compatible  # only if you need to support Python 2
class Menu(models.Model):
    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    food_cal = models.IntegerField(default=0)
    food_name = models.CharField(max_length=200)
    
    def __str__(self):
        return self.food_name
    
    