from django.apps import AppConfig


class RandomfoodConfig(AppConfig):
    name = 'randomfood'
