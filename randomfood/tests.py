from django.test import TestCase

# Create your tests here.
class ListViewTest(TestCase):

    def test_home_template(self):
        response = self.client.get('/randomfood/')
        self.assertTemplateUsed(response, 'randomfood/home.html')