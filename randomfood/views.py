from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Food,Menu
import random


    
def fill_detail(request):
    lists = Food.objects.order_by('pub_date')
    context = {'lists':lists}
    return render(request, 'randomfood/fill_detail_page.html',context)
    
def random_food_by_type(request):
    food_id = request.POST['type']
    food_type = Food.objects.get(pk=food_id)
    menu_set = food_type.menu_set.all()
    result_id = random.randrange(1,len(menu_set),1)
    menu =  menu_set[result_id]
    cal = menu_set[result_id].food_cal
    context = {'menu':menu,'cal':cal}
    return render(request, 'randomfood/detail_page.html',context)

def edit_list(request):
    lists = Food.objects.order_by('pub_date')
    context = {'lists':lists}
    return render(request, 'randomfood/edit_page.html',context)
    
def edit_option(request):
    lists = Food.objects.order_by('pub_date')
    food_id = request.POST['type']
    food_type = Food.objects.get(pk=food_id)
    food_type_text = food_type.food_text
    menu_set = food_type.menu_set.all()
    
    context = {'menu_set':menu_set, 'food_type_text':food_type_text, 'food_id':food_id}
    return render(request, 'randomfood/option_page.html',context)

def add_list(request,food_id):
    lists = Food.objects.order_by('pub_date')
    food_type = Food.objects.get(pk=food_id)
    food_type_text = food_type.food_text
    menu_set = food_type.menu_set.all()

    name = request.POST['menu_name']
    cal = request.POST['cal']
    q = Food.objects.get(pk=food_id)
    q.menu_set.create(food_cal=cal,food_name=name)
    q.save()
    
    context = {'menu_set':menu_set, 'food_type_text':food_type_text, 'food_id':food_id}
    return render(request, 'randomfood/option_page.html',context)

def del_list(request,food_id):
    food_type = Food.objects.get(pk=food_id)
    food_type_text = food_type.food_text
    menu_id = request.POST['menu_id']
    menu_set = food_type.menu_set.all()
    
    get_menu = Menu.objects.get(id=menu_id)
    get_menu.delete()
    
    context = {'menu_set':menu_set, 'food_type_text':food_type_text, 'food_id':food_id}
    return render(request, 'randomfood/option_page.html',context)